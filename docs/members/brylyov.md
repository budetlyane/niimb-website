---
sidebar_position: 1
---

# Брылёв Игорь Николаевич

Исследователь проблем и перспектив автоматизации, [igor-brilev@yandex.ru](mailto:igor-brilev@yandex.ru)

![Брылёв Игорь Николаевич](img/brylyov_small.jpg)

Основатель и руководитель проекта по разработке общедоступных средств роботизации «Робосборщик». Автор и ведущий научно-популярного видео-канала о проектировании социально-технических систем будущего «будь и точка».

## Веб-сайты
- Веб-сайт проекта Робосборщик - [robossembler.org](https://robossembler.org)
- Профили на платформах открытого кода - [gitlab](https://gitlab.com/movefasta), [github](https://github.com/movefasta)
- Видео-канал «будь и точка» на [Youtube](https://www.youtube.com/@upravmir)

## Публикации
### Статьи
- [Экономика автоматизации](https://docs.google.com/document/d/1a3BfynOQflpeMwhUgWMBKUPh6jE7KuE3i0x79RBQNo8/edit?usp=drivesdk)
### Видео  
- [Экономика автоматизации](https://www.youtube.com/watch?v=Z9m_2H4a6xw)
- [Децентрализация в природе, обществе и интернете](https://www.youtube.com/watch?v=YEBvuY_WrY0)
- [Социальная архитектура сообществ вокруг проектов разработки открытого ПО](https://www.youtube.com/watch?v=wqlhYYtyRPI)
- [Робосфера. Стратегия роботизации России](https://youtu.be/hl94SpfT9wY?t=350)

## Образ будущего

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/RErVxHKHpMQ?si=Nc8uGbi8rzhYMbWy&amp;start=59" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>