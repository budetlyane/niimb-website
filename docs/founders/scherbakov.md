---
sidebar_position: 4
---

# Щербаков Андрей Владимирович 

Заместитель по экономическому моделированию будущего, [аndrey-iron1@yandex.ru](mailto:andrey-iron1@yandex.ru)

![Щербаков Андрей Владимирович](/img/scherbakov.jpg)

Кандидат экономических наук (1999). Основатель (1990) и генеральный директор компании «Курс» (ООО «НПО «Курс»), предприятия-производителя насосного оборудования мирового уровня. Директор Института социально-экономического прогнозирования им. Д.И. Менделеева. Спикер Сретенского клуба им. С.П. Курдюмова. Автор более ста научных работ по математическому моделированию социально-экономических процессов.  

## Публикации

### Статьи  
- [Динамическая модель закрытого общества (институциональные ловушки и кризисы)](http://mendeleev-center.ru/articles/article-0018.html)  
- [Доллар. Окно возможностей и пирамида](http://mendeleev-center.ru/articles/article-0007.html)  
- [Мир без доллара и ФРС](http://mendeleev-center.ru/articles/article-0006.html)  
- [Модель социальной устойчивости общества](http://mendeleev-center.ru/articles/article-0201.html)  
- [Цифровой рубль, как способ вывода России из экономической стагнации](http://mendeleev-center.ru/articles/article-0229.html)  
### Книги
- [Мобилизационная экономика России](http://mendeleev-center.ru/research/research-0288.html)
- [Хроника Квантового перехода](http://mendeleev-center.ru/research/research-0279.html)
- [Дорожная карта - вызовы ближайшего десятилетия](http://mendeleev-center.ru/research/research-0198.html)  
- [Сетевая экономика](http://mendeleev-center.ru/research/research-0152.html)  
- [Новый общественный договор](http://mendeleev-center.ru/research/research-0136.html)  
- [Экономика России. Математическая модель](http://mendeleev-center.ru/research/research-0096.html)  
- [Введение в теорию цифровой экономики](http://mendeleev-center.ru/research/research-0085.html)  
- [Россия 2112](http://mendeleev-center.ru/research/research-0082.html)  
- [Естественно-научная концепция в теоретической экономике](http://mendeleev-center.ru/research/research-0066.html)  
- [Мифология Адама Смита (о чем на самом деле книга «Исследование о природе и причинах богатства народов»)](http://mendeleev-center.ru/research/research-0067.html)  
### Сборники с участием автора
- [Социально-экономический бюллетень 2022](http://mendeleev-center.ru/research/research-0301.html)  
- [Социально-экономический бюллетень 2021](http://mendeleev-center.ru/research/research-0215.html)  
- [Социально-экономический бюллетень 2020](http://mendeleev-center.ru/research/research-0159.html)  
- [Социально-Экономический бюллетень 2019](http://mendeleev-center.ru/research/research-0097.html)  
- [Социально-экономический бюллетень 2017](http://mendeleev-center.ru/research/research-0084.html)  
- [Социально-экономический бюллетень 2016](http://mendeleev-center.ru/research/research-0072.html)  
- [Социально-экономический бюллетень 2015](http://mendeleev-center.ru/research/research-0019.html)  