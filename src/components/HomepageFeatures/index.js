import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Фёдор Иванович Тютчев',
    imageUrl: require('@site/static/img/tytchev.jpg').default,
    description: (
      <>
      </>
    ),
  },
  {
    title: '',
    description: (
      <>
      <h2><i>Нам не дано предугадать,</i></h2>
      <h2><i>Как слово наше отзовется,</i></h2>
      <h2><i>И нам сочувствие дается,</i></h2>
      <h2><i>Как нам дается благодать…</i></h2>
      <h2>-</h2>
      <h2><i>1869 г.</i></h2>
      </>
    ),
  },
  // {
  //   title: 'Powered by React',
  //   imageUrl: require('@site/static/img/tytchev.jpg').default,
  //   description: (
  //     <>
        
  //     </>
  //   ),
  // },
];

function Feature({imageUrl, title, description}) {
  return (
    <div className={clsx('col col--6')}>
      <div className="text--center">
      <img className={styles.featureImage} src={imageUrl} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
