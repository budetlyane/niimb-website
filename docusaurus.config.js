// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'НИИМБ',
  tagline: 'Научно-Исследовательский Институт Моделирования Будущего',
  url: 'https://your-docusaurus-test-site.com',
  baseUrl: '/',
  staticDirectories: ['public', 'static'],
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'ru',
    locales: ['en', 'ru'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          // editUrl:
          //   'https://gitlab.com/niimb/niimb.gitlab.io/tree/main/',
        },
        blog: {
          showReadingTime: true,
          // routeBasePath: '/',
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          // editUrl:
          //   'https://gitlab.com/niimb/niimb.gitlab.io/tree/main/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'НИИМБ',
        logo: {
          alt: 'НИИ Моделирования Будущего',
          src: 'img/logo_video.png',
        },
        items: [
          {
            type: 'doc',
            docId: '/category/основатели-ниимб',
            position: 'left',
            label: 'О нас',
          },
          {to: '/blog', label: 'Публикации', position: 'left'},
          {to: '/manifesto', label: 'Манифест', position: 'left'},
          // {to: '/community-foresight', label: 'Образы будущего', position: 'left'},
          {
            type: 'doc',
            docId: '/category/образы-будущего',
            position: 'left',
            label: 'Образы будущего',
          },
          {
            href: 'https://www.youtube.com/channel/UCle0VJr6KCFT2gcLeHU-Qqg',
            label: 'Youtube',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'О нас',
            items: [
              {
                label: 'Манифест',
                to: 'manifesto',
              },
            ],
          },
          {
            title: 'Мы в СМИ',
            items: [
              {
                label: 'Канал на Youtube',
                href: 'https://www.youtube.com/channel/UCle0VJr6KCFT2gcLeHU-Qqg',
              },
            ],
          },
          {
            title: 'Свяжитесь с нами',
            items: [
              {
                label: 'По электронной почте',
                href: 'mailto:niimb@inbox.ru',
              },
            ],
          },
        ],
        copyright: `Все права защищены © ${new Date().getFullYear()} НИИМБ`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
